<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';

    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');    
    }

    public function getNameAttribute ($value)
        {
           return $this->firstName.' '.$this->lastName;
        }
}
