<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team_match extends Model
{
    protected $table = 'team_matches';

     public function match()
    {
        return $this->hasOne('App\Match','id','match_id');    
    }

     public function teamOne()
    {
        return $this->hasOne('App\Team','id','team_one_id');    
    }

     public function teamSecond()
    {
        return $this->hasOne('App\Team','id','team_two_id');    
    }

    public function getScheduledDateAttribute ($value)
        {
           return date('d-m-Y',strtotime($this->attributes['scheduled_at']));
        }

          public function getCompletedDateAttribute ($value)
        {
           return date('d-m-Y',strtotime($this->attributes['completed_at']));
        }

         public function getWinnerTeamAttribute ($value)
        {
           return $winner=$this->attributes['winner_team_id'];
        }

}
