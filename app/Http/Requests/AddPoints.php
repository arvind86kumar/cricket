<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPoints extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_one_id'=>'required',
            'team_two_id'=>'required',
            'match_id'=>'required',
            'point_team_one'=>'required',
            'point_team_two'=>'required',

        ];
    }

    public function messages(){
        return [
            'team_one_id.required'=>'Please choose team one',
            'team_two_id.required'=>'Please choose second team',
            'match_id.required'=>'Pelase choose Match',
            'point_team_one.required'=>'Please enter points team one',
            'point_team_two.required'=>'Please enter points team second',

        ];
    }
}
