<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPlayer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName'=>'required',
            'lastName'=>'required',
            'playerJersyNumber'=>'required',
            'country_id'=>'required',
            'team_id'=>'required',
            'profile_pic'=>'required'

        ];
    }

    public function messages(){
        return['firstName.required'=>'Please enter First Name',
                'lastName.required'=>'Please enter last Name',
                'country_id.required'=>'Please choose country',
                'team_id.required'=>'Please choose team',
                'playerJersyNumber.required'=>'Please enter player jersy number',
                'profile_pic.required'=>'Please choose Profile Pic'];
        }
}
