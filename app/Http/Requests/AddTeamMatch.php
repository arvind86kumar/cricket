<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddTeamMatch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match_id'=>'required',
            'scheduled_at'=>'required',
            'completed_at'=>'required',
            'team_one_id'=>'required',
            'team_two_id'=>'required'
        ];
    }

    public function messages(){
       return [ 'match_id.required'=>'Please select Match',
        'scheduled_at.required'=>'Choose scheduled Date',
        'completed_at.required'=>'Choose Completed Date',
        'team_one_id.required'=>'Choose Team One',
        'team_two_id.required'=>'Choose Team Second'
        ];
    }
}
