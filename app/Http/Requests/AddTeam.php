<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddTeam extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>'required',
            'club_state'=>'required',
            'country_id'=>'required|unique:teams',
             'logo_uri' =>'required|mimes:jpg,png,jpeg',
        ];
    }

    public function messages(){
        return['name.required'=>'Team name is required',
                'club_state.required'=>'Please enter club state',
                'country_id.required'=>'Please choose country',
                'country_id.unique'=>'Team with this country is already exist',
                'logo_uri.required'=>'Please choose Logo'];
        }

}