<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Country,Team};
use App\Http\Requests\AddTeam;

class TeamController extends Controller
{
    private $pageNumber=20;
    public function index(){
        $teams=Team::with(['country'])->get();
        return view('backend.team.list',compact('teams'));
    }

    public function addTeam(){
    	$countries=Country::all();
        return view('backend.team.add',compact('countries'));
    }

    public function save(AddTeam $request)
    {
        $imageName = time().'.'.$request->logo_uri->getClientOriginalExtension();
        $request->logo_uri->move(public_path('/team_logo'), $imageName);
        if ($request->file('logo_uri')!=null)
        {
        $team           	= new Team();
        $team->name    		= $request->name;
        $team->club_state  	= $request->club_state; 
        $team->country_id   = $request->country_id;
        $team->logo_uri=$imageName;
        $team->save();        
         return redirect("/add-team")->with('success', 'New Team added successfully.');
     }
    }
    
}
