<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Match;
use App\Http\Requests\AddMatch;
class MatchController extends Controller
{
    public function index(){
    	$matchs=Match::all();
        return view('backend.match.list',compact('matchs'));
    }

    public function add(){
        return view('backend.match.add');
    }

    public function save(AddMatch $request)
    {   
        $match           	= new Match();
        $match->name    		= $request->name;
        $match->save();        
         return redirect("/add-new-match")->with('success', 'New Match added successfully.');
    
    }
}
