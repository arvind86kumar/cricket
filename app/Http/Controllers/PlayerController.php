<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Country,Team,Player};
use App\Http\Requests\AddPlayer;
class PlayerController extends Controller
{
    public function index(){
        $players=Player::with(['country'])->get();
        return view('backend.player.list',compact('players'));
    }

     public function add(){
    	$countries=Country::all();
    	$teams 	=Team::all();
        return view('backend.player.add',compact('countries','teams'));
    }

    public function save(AddPlayer $request)
    {
        $imageName = time().'.'.$request->profile_pic->getClientOriginalExtension();
        $request->profile_pic->move(public_path('/players_pic'), $imageName);
           if ($request->file('profile_pic')!=null)
        {
        $player           			= new Player();
        $player->firstName    		= $request->firstName;
        $player->lastName    		= $request->lastName;
        $player->playerJersyNumber  = $request->playerJersyNumber;
        $player->team_id    		= $request->team_id;
        $player->country_id         = $request->country_id;
        $player->imageUri           =$imageName;
        $player->save();        
         return redirect("/add-player")->with('success', 'New player added successfully.');
     	}else{
     		return redirect("/add-player")->with('error', 'Unable to add player record.');
     	}
    }

    public function team_player(){
       $team_id= $_GET['team_id'];
        $players=Player::where(['team_id'=>$team_id])->get();
        
        return view('backend.player.player-team',compact('players'));
    }
    
}
