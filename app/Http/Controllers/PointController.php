<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddPoints;
use App\{Point,Match,Team};
class PointController extends Controller
{
    public function index(){
    	$points=Point::with(['match','teamOne','teamSecond'])->get();
    	
        return view('backend.points.list',compact('points'));
    }

    public function add(){
    	$matchs=Match::all();
    	$teams=Team::all();
        return view('backend.points.add',compact('matchs','teams'));
    }

    public function save(AddPoints $request)
    {
        
        $point           	= new Point();
        $point->match_id    = $request->match_id;
        $point->team_one_id  = $request->team_one_id; 
        $point->team_two_id   = $request->team_two_id;
        $point->point_team_one= $request->point_team_one;
        $point->point_team_two= $request->point_team_two;
        $point->save();        
         return redirect("/add-team")->with('success', 'Team points added successfully.');
     
    }
}
