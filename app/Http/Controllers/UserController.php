<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
class UserController extends Controller
{
    public function authenticate(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' =>$request->password])) {
              return redirect()->intended('add-team');
          }else
          {            
             return redirect('/')->with('error', 'Please check your login details again.');
          }
      }
      
      
      public function logout() {
           Auth::logout();
          return redirect('/')->with('success', 'You have successfully logged out.');
        }


    public function dashboard(){

    }
  
}
