<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddTeamMatch;
use App\{Match,Team,Team_match};
class TeamMatchController extends Controller
{
    
	public function index(){
		$team_matchs=Team_match::with(['teamOne','teamSecond','match'])->get();
		return view('backend.team-match.list',compact('team_matchs'));
	}

     public function add(){
    	$matchs = Match::all();
    	$teams  = Team::all();
        return view('backend.team-match.add',compact('matchs','teams'));
    }

    public function save(AddTeamMatch $request)
    { 
        $team_match           			= new Team_match();
        $team_match->match_id    		= $request->match_id;
        $team_match->scheduled_at    	=date('Y-m-d H:i:s',strtotime($request->scheduled_at)) ;
        $team_match->completed_at    	= date('Y-m-d H:i:s',strtotime($request->completed_at)) ;
        $team_match->venue    			= $request->venue;
        $team_match->status    			= $request->status;
        $team_match->team_one_id    	= $request->team_one_id;
        $team_match->team_two_id    	= $request->team_two_id;
        $team_match->winner_team_id     = $request->winner_team_id;
        $team_match->save();        
         return redirect("/add-new-team-match")->with('success', 'New Team Match added successfully.');
    }
}
