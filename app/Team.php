<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');    
    }
	public function setNameAttribute ($value)
    {
        $this->attributes['name'] = ucfirst($value);
	}
	
    
}
