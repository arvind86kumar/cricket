<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $table = 'points';
      public function teamOne()
    {
        return $this->hasOne('App\Team','id','team_one_id');    
    }

      public function teamSecond()
    {
        return $this->hasOne('App\Team','id','team_two_id');    
    }

       public function match()
    {
        return $this->hasOne('App\Match','id','match_id');    
    }
}
