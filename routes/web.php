<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('backend/admin-login');
});

Route::post('authenticatee', ['as' => 'authenticatse', 'uses' => 'UserController@authenticate'] );



 
Route::middleware(['AdminCheck'])->group(function () {
    Route::get('add-team','TeamController@addTeam');
 	Route::post('save','TeamController@save');
 	Route::get('team-list','TeamController@index');
 	Route::get('add-player','PlayerController@add');
 	Route::post('save-player','PlayerController@save');
 	Route::get('player-list','PlayerController@index');
 	Route::get('player-team','PlayerController@team_player');
 	Route::get('add-new-match','MatchController@add');
 	Route::post('save-match','MatchController@save');
 	Route::get('match-list','MatchController@index');
 	Route::get('match-list','TeamMatchController@add');
 	Route::get('add-new-team-match','TeamMatchController@add');
 	Route::post('save-team-match','TeamMatchController@save');
 	Route::get('team-match','TeamMatchController@index');
 	Route::get('add-team-points','PointController@add');
 	Route::post('save-team-points','PointController@save');
 	Route::get('team-points','PointController@index');
 	Route::get('/logout','UserController@logout');
});