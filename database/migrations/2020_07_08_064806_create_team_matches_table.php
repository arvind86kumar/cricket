<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_matches', function (Blueprint $table) {
            $table->id();
            $table->integer('match_id');
            $table->dateTime('scheduled_at', 0);
            $table->dateTime('completed_at', 0)->nullable();
            $table->string('venue');
            $table->enum('status', ['Not Start', 'Completed','Cancelled','Match Draw']);
            $table->integer('winner_team_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_matches');
    }
}
