@include('layouts.admin.header')
  <div class="content-wrapper">
	@include('layouts.flash-message')
	@yield('content')
</div>
@include('layouts.admin.footer')