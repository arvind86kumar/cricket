@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.add-team') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.add-team') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.add-team') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">{{ __('admin.add-team') }}</h3>
               
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" enctype="multipart/form-data" method="post" action="{{ action('TeamController@save') }}">
                   {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Team Name</label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" id="exampleInputEmail1" placeholder="Enter Team Name">
                  </div>
                 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Country</label>
                    <select class="form-control" name="country_id">
                        <option value="">--Select Country---</option>
                        @foreach($countries as $coun)
                        <option value="{{$coun->id}}">{{$coun->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  
                  
                   <div class="form-group">
                    <label for="exampleInputEmail1">Club State</label>
                    <input type="text" name="club_state" class="form-control" id="exampleInputEmail1" placeholder="Enter Club State">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Upload Logo</label>
                    <input type="file" name="logo_uri"  class="form-control" id="exampleInputEmail1" placeholder="Upload Logo">
                  </div>
                  
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
  @endsection 