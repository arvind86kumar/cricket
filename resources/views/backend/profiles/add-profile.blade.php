@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.add-new-ad') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.add-new-ad') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.add-new-ad') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">Add Basic Information</h3>
                <p class="alert alert-info">Note: <blink>A new digital ad validity is 200 days from publish date.</blink></p>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{ action('Admin\ProfileController@save') }}">
                   {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Ads Title</label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}" id="exampleInputEmail1" placeholder="Enter Ads Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea class="form-control textarea" value="{{old('description')}}" name="description" placeholder="Enter desciption"></textarea>
                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Category</label>
                    <select class="form-control" name="category_id">
                        <option value="">--Select Category---</option>
                        @foreach($categories as $cat)
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">City</label>
                    <select class="form-control" name="city_id">
                        <option value="">--Select City---</option>
                         @foreach($locations as $loc)
                        <option value="{{$loc->id}}">{{$loc->name}}</option>
                        @endforeach
                    </select>
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <textarea class="form-control" value="{{old('address')}}" name="address" placeholder="Enter Address"></textarea>
                    
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Near By Location</label>
                    <input type="text" name="area" class="form-control" id="exampleInputEmail1" placeholder="Enter Near By Location">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Zip Code</label>
                    <input type="text" name="zip" value="{{old('zip')}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Zip">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contact Number</label>
                    <input type="text" name="phone" value="{{old('phone')}}" class="form-control" id="exampleInputPassword1" placeholder="Enter Contact Number">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputPassword1">Youtube Url</label>
                    <input type="text" name="youtube_url" value="{{old('youtube_url')}}" class="form-control" id="exampleInputPassword1" placeholder="Enter Youtube Embed Url">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputPassword1">Payable Amount</label>
                    <h2>Rs. 199</h2></h2>
                    <p>Take Payment from user.</p>
                  </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Continue</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
@push('style')
<!-- DataTables -->
      <link rel="stylesheet" href="{{asset('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endpush

@push('script')
<script type="text/javascript" src="{{asset('public/admin/bower_components/ckeditor/ckeditor.js')}}"></script>
<script>
  $(function () {   
    CKEDITOR.replace('description'); 
  })
</script>
@endpush
  @endsection 