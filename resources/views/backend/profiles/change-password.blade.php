@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.change-pass') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.change-pass') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.change-pass') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">{{ __('admin.change-pass') }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype='multipart/form-data'  action="{{ action('Admin\ProfileController@passChange') }}">
                   {{ csrf_field() }}
                <div class="card-body">                    
                  <div class="form-group">
                    <label for="exampleInputEmail1">{{ __('admin.new-pass')}}</label>
                    <input  type="password" name="new_password" class="form-control" value="" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">{{ __('admin.confirm-pass') }}</label>
                    <input type="password" name="confirm_password" class="form-control">
                  </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">{{ __('admin.submit') }}</button>
                  <button type="submit" class="btn btn-default float-right">{{ __('admin.cancel') }}</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->


  @endsection 