@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.all-ads') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.all-ads') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.all-ads') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                    <th width="30%" >Ads Title</th>
                    <th width=10%" class="sorting" >Category</th>
                    <th width="10%" class="sorting" >City/Location</th>
                  <th width="10%"  class="sorting" tabindex="0" >Mobile</th>
                  <th width="10%"  class="sorting" tabindex="0">Created Date</th>
                  <th width="10%"> Status</th>
                  <th width="20%" >Tasks</th>
                  </tr>
                </thead>
                <tbody>
                  @if(!empty($profiles))
                @foreach($profiles as $profile)
                <tr role="row" class="odd">
                  <td>{{$profile->name}}</td>
                  <td>{{$profile->categories->name}}</td>
                  <td>{{$profile->cityName->name}}</td>
                  <td>{{$profile->phone}}</td>
                  <td>{{date( 'd-M-Y', strtotime($profile->created_at))}}</td>
                  <td>{{$profile->status}}</td>
                  <td><a title="View" target="_new" href="{{url($profile->profile_slug)}}" class="btn btn-success"><i class="fa fa-eye" aria-hidden="true"></i>
</a>&nbsp;<a title="Edit" class="btn btn-info" href="edit-ad?id={{base64_encode($profile->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                  <a title="Delete" href="delete-ad?id={{base64_encode($profile->id) }}" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                
                  </td>
                </tr>
                @endforeach
                @else
                <tr><td colspan="5">No data found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr role="row">
                    <th class="sorting_asc" tabindex="0" >Ads Title</th>
                    <th class="sorting" >Category</th>
                    <th class="sorting" >City/Location</th>
                  <th class="sorting" tabindex="0" >Mobile</th>
                  <th class="sorting" tabindex="0">Created Date</th>
                  <th>Status</th>
                  <th class="sorting" tabindex="0">Tasks</th>
                  </tr>
                </tfoot>
              </table>
               {{ $profiles->links('inc.pagination') }}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
@push('style')
<!-- DataTables -->
      <link rel="stylesheet" href="{{asset('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('script')
<!-- DataTables -->
<script src="{{asset('public/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/bower_components/datatables.net/js/dataTables.bootstrap.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example2').DataTable()
   
  })
</script>
@endpush
  @endsection 