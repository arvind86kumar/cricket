@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.edit-pic') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.edit-pic') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.edit-pic') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">Edit Pictures</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype='multipart/form-data'  action="{{ action('Admin\ProfileController@save_images') }}">
                   {{ csrf_field() }}
                <div class="card-body">
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>"
                  <div class="form-group">
                    <label for="exampleInputEmail1">Add Image</label>
                    <input type="file" name="image[]" class="form-control"  id="exampleInputEmail1" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Add Image</label>
                    <input type="file" name="image[]" class="form-control"  id="exampleInputEmail1">
                  </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Save</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
  @endsection 