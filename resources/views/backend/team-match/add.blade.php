@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.team-match') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.team-match') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.team-match') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">{{ __('admin.team-match') }}</h3>
               
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" enctype="multipart/form-data" method="post" action="{{ action('TeamMatchController@save') }}">
                   {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Match</label>
                    <select class="form-control" name="match_id">
                        <option value="">--Select Match---</option>
                        @foreach($matchs as $match)
                        <option value="{{$match->id}}">{{$match->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Scheduled Date</label>
                    <input type="text" name="scheduled_at" class="form-control" value="{{old('scheduled_at')}}" id="scheduled_at" placeholder="Choose Scheduled Date">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Completed Date</label>
                    <input type="text" name="completed_at" class="form-control" value="{{old('completed_at')}}" id="completed_at" placeholder="Choose completed Date">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Venue</label>
                    <input type="text" name="venue" class="form-control" id="exampleInputEmail1" placeholder="Enter Club State">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Status</label>
                   <select class="form-control" name="status">
                        <option value="">--Select Status---</option>
                        <option value="Not Start">Not Start</option>
                        <option value="Completed">Completed</option>
                        <option value="Cancelled">Cancelled</option>
                        <option value="Match Draw">Match Draw</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Winnder Team</label>
                   <select class="form-control" name="winner_team_id">
                        <option value="">--Select Winner Team---</option>
                        @foreach($teams as $team)
                        <option value="{{$team->id}}">{{$team->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Team One </label>
                   <select class="form-control" name="team_one_id">
                        <option value="">--Select Team One---</option>
                         @foreach($teams as $team)
                        <option value="{{$team->id}}">{{$team->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Team Two </label>
                   <select class="form-control" name="team_two_id">
                        <option value="">--Select Second Team---</option>
                          @foreach($teams as $team)
                        <option value="{{$team->id}}">{{$team->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
@push('script')
<!-- DataTables -->

<!-- page script -->
<script>
  $(function () {
    $('#scheduled_at').datepicker({
      autoclose: true
    });
     $('#completed_at').datepicker({
      autoclose: true
    });
   
  })
</script>
@endpush
  @endsection 