@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.team-point') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.team-point') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.team-point') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                  <th width="10%" >S.No</th>
                    <th width="10%" >Match Name</th>
                    <th width="10%" >Team One</th>
                    <th width="10%" >Team Second</th>
                    <th width="10%" >Team One Point</th>
                    <th width="10%" >Team Second Point</th>
                  
                  </tr>
                </thead>
                <tbody>
                  @if(!empty($points))
                @foreach($points as $match)
                <tr role="row" class="odd">
                  <td>{{$loop->iteration}}</td>
                  <td>{{$match->match->name}}</td>
                  <td>{{$match->teamOne->name}}</td>
                  <td>{{$match->teamSecond->name}}</td>
                  <td>{{$match->point_team_one}}</td>
                  <td>{{$match->point_team_two}}</td>
              
                 
                  
                </tr>
                @endforeach
                @else
                <tr><td colspan="5">No data found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr role="row">
                   <th width="10%" >S.No</th>
                    <th width="10%" >Match Name</th>
                    <th width="10%" >Team One</th>
                    <th width="10%" >Team Second</th>
                    <th width="10%" >Team One Point</th>
                    <th width="10%" >Team Second Point</th>
                  </tr>
                </tfoot>
              </table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
@push('style')
<!-- DataTables -->
      <link rel="stylesheet" href="{{asset('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('script')
<!-- DataTables -->
<script src="{{asset('public/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/bower_components/datatables.net/js/dataTables.bootstrap.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example2').DataTable()
   
  })
</script>
@endpush
  @endsection 