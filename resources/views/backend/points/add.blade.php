@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.add-team-point') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.add-team-point') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.add-team-point') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">{{ __('admin.add-team-point') }}</h3>
               
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="{{ action('PointController@save') }}">
                   {{ csrf_field() }}
                <div class="card-body">
                   <div class="form-group">
                    <label for="exampleInputEmail1">Match</label>
                    <select class="form-control" name="match_id">
                        <option value="">--Select Match---</option>
                        @foreach($matchs as $match)
                        <option value="{{$match->id}}">{{$match->name}}</option>
                        @endforeach
                    </select>
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Team One</label>
                    <select class="form-control" name="team_one_id">
                        <option value="">--Select Team One---</option>
                        @foreach($teams as $team)
                        <option value="{{$team->id}}">{{$team->name}}</option>
                        @endforeach
                    </select>
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Team Second</label>
                    <select class="form-control" name="team_two_id">
                          <option value="">--Select Team Second---</option>
                        @foreach($teams as $team)
                        <option value="{{$team->id}}">{{$team->name}}</option>
                        @endforeach
                    </select>
                  </div>   
                  <div class="form-group">
                    <label for="exampleInputEmail1">Point Team One</label>
                    <input type="text" name="point_team_one" class="form-control" value="{{old('point_team_one')}}" id="exampleInputEmail1" placeholder="Enter Team one Points">
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Point Team Two</label>
                    <input type="text" name="point_team_two" class="form-control" value="{{old('point_team_two')}}" id="exampleInputEmail1" placeholder="Enter Team second Points">
                  </div>                 
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
  @endsection 