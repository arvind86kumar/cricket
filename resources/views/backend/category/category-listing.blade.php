@extends('admin')
@section('content')
<section class="content-header">
      <h1>
      {{ __('admin.category_listing') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.category_listing') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.category_listing') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row"><th>S.No</th><th class="sorting_asc" tabindex="0" >Category Name</th></tr>
                </thead>
                <tbody>
                  @if(!empty($categories))
                @foreach($categories as $category)
                <tr role="row" class="odd">
                    <th>{{$loop->iteration }}</th>
                  <td class="sorting_1">{{$category->name}}</td>
                  
                  
                </tr>
                @endforeach
                @else
                <tr><td colspan="5">No data found</td></tr>
                @endif
                </tbody>
               
              </table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
@push('style')
<!-- DataTables -->
      <link rel="stylesheet" href="{{asset('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('script')
<!-- DataTables -->
<script src="{{asset('public/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/bower_components/datatables.net/js/dataTables.bootstrap.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example2').DataTable()
   
  })
</script>
@endpush
  @endsection 