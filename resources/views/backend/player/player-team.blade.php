@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.player-team') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.player-team') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.player-team') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                <tr role="row">
                  <th width="30%" >Profile Pic</th>
                    <th width="20%" >Name</th>
                    <th width="20%" class="sorting" >Country</th>
                
                  </tr>
                </thead>
                <tbody>
                  @if(!empty($players))
                @foreach($players as $player)
                <tr role="row" class="odd">
                  <td><img width="100px" height="120px" class="img-circle img-responsive" title="" src="public/players_pic/{{$player->imageUri}}"></td>
                  <td>{{$player->name}}</td>
                  <td>{{$player->country->name}}</td>
                 
                 
                </tr>
                @endforeach
                @else
                <tr><td colspan="5">No data found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr role="row">
                  <th class="sorting" >Profile Pic</th>
                    <th class="sorting_asc" tabindex="0" > Name</th>
                    <th class="sorting" >Country</th>
                 
                  </tr>
                </tfoot>
              </table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
@push('style')
<!-- DataTables -->
      <link rel="stylesheet" href="{{asset('public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('script')
<!-- DataTables -->
<script src="{{asset('public/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/bower_components/datatables.net/js/dataTables.bootstrap.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example2').DataTable()
   
  })
</script>
@endpush
  @endsection 