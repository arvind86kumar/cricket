@extends('admin')
@section('content')
<section class="content-header">
      <h1>
       {{ __('admin.add-player') }}       
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('dashboard.html')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ __('admin.add-player') }}</li>
      </ol>
    </section>
<!--Manin Content-->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">{{ __('admin.add-player') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card card-success">
                <h3 class="card-title">{{ __('admin.add-player') }}</h3>
               
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" enctype="multipart/form-data" method="post" action="{{ action('PlayerController@save') }}">
                   {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" name="firstName" class="form-control" value="{{old('firstName')}}" id="exampleInputEmail1" placeholder="Enter First Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" name="lastName" class="form-control" value="{{old('lastName')}}" id="exampleInputEmail1" placeholder="Enter Last Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Player Jersy Number</label>
                    <input type="text" name="playerJersyNumber" class="form-control" value="{{old('playerJersyNumber')}}" id="exampleInputEmail1" placeholder="Enter Player Jersy Number">
                  </div>
                 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Country</label>
                    <select class="form-control" name="country_id">
                        <option value="">--Select Country---</option>
                        @foreach($countries as $coun)
                        <option value="{{$coun->id}}">{{$coun->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Choose Team </label>
                    <select class="form-control" name="team_id">
                        <option value="">--Select Team---</option>
                        @foreach($teams as $team)
                        <option value="{{$team->id}}">{{$team->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  
                   <div class="form-group">
                    <label for="exampleInputEmail1">Upload Profile Pic</label>
                    <input type="file" name="profile_pic"  class="form-control" id="exampleInputEmail1" placeholder="Upload Profile Pic">
                  </div>
                  
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Submit</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- End Content-->
  @endsection 