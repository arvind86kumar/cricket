<?php

return [
'add-team'			=>'Add New Team',
'add-player'		=>'Add New Player',
'team-list'			=>'Team List',
'player-list'		=>'Player List',
'add-match'			=>'Add New Match',
'match-list'		=>'Match List',
'player-team'		=>'Player Team',
'team-match'		=>'Team Match',
'team-match-list'   =>'All Team Match',
'add-team-point'    =>'Add Team Points',
'team-point'		=>'Team Point'
];